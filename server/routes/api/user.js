"use stirct";
const Router = require("koa-router");
const userRoute = new Router();
const { userController } = require("../../controller");

/**新系统首次注册管理员(只能使用一次) */
userRoute.post("/admin", userController.postAdmin);
/**用户登录 */
userRoute.post("/login", userController.userLogin);
/**修改密码 */
userRoute.put("/passwd", userController.updatePass);

module.exports = userRoute;

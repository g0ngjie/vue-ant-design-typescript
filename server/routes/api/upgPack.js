"use stirct";
const Router = require("koa-router");
const upgPackRoute = new Router();
const koaBody = require("koa-body");
const { upgPackController } = require("../../controller");
const { auth } = require("../../middlewares");

/**获取升级策略信息 */
upgPackRoute.post(
  "/getStrategyInfo",
  auth.validateupgPack,
  upgPackController.getStrategyInfo
);

/**获取服务端安装包最新版本信息 */
upgPackRoute.post(
  "/getLatestVerInfo",
  auth.validateupgPack,
  upgPackController.getLatestVerInfo
);

/**下载程序升级包 */
upgPackRoute.post(
  "/getUpgradePack",
  auth.validateupgPack,
  upgPackController.getUpgradePack
);

/**下载升级包结果通知服务端 */
upgPackRoute.post(
  "/notifyDownRes",
  auth.validateupgPack,
  upgPackController.notifyDownRes
);

/**上传驱动安装日志 */
upgPackRoute.post(
  "/upDevInstallRes",
  koaBody({
    multipart: true,
    formidable: { maxFileSize: 20 * 1024 * 1024, multipart: true }
  }),
  auth.validateupgPack,
  upgPackController.upDevInstallRes
);

/**获取服务器信息 */
upgPackRoute.get("/getServerInfo", upgPackController.getServerInfo);

module.exports = upgPackRoute;

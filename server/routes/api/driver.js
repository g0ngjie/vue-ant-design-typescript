"use stirct";
const Router = require("koa-router");
const driverRoute = new Router();
const koaBody = require("koa-body");
const { driverController } = require("../../controller");

// /**分片上传驱动 */
// driverRoute.post(
//   "/",
//   koaBody({
//     multipart: true,
//     formidable: { maxFileSize: 20 * 1024 * 1024, multipart: true }
//   }),
//   driverController.uploadDriver
// );
// /**合并分片内容，成为完整驱动 */
// driverRoute.post("/merge", driverController.mergeDriver);

/**测试服务器是否连通 */
driverRoute.get("/test", async ctx => {
  ctx.body = { msg: "ok", errNo: 0 };
  ctx.status = 200;
});
/**大文件上传 */
driverRoute.post(
  "/huge",
  koaBody({
    multipart: true,
    formidable: { maxFileSize: 1024 * 1024 * 1024, multipart: true }
  }),
  driverController.uploadHugeDriver
);
/**测试上传的文件是否完成 */
driverRoute.post("/finish", driverController.checkFinish);
/**设置驱动相关信息 */
driverRoute.post("/upload", driverController.setDriver);
/**编辑驱动的相关信息 */
driverRoute.put("/upload/:did", driverController.updateDriver);
/**删除驱动 */
driverRoute.delete("/upload/:did", driverController.removeDriver);
/**获取驱动列表 以及获取最新驱动(isNewest: true) */
driverRoute.post("/drivers", driverController.getDrivers);
/**获取驱动列表数目 */
driverRoute.post("/drivers/count", driverController.countDrivers);
/**获取安装列表 */
driverRoute.post("/installlogs", driverController.getInstallLogs);
/**获取安装版本 */
driverRoute.post("/installversions", driverController.getVersions);
/**计数安装列表 */
driverRoute.post("/installlogs/count", driverController.countInstallLogs);

module.exports = driverRoute;

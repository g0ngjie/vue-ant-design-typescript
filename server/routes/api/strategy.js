"use stirct";
const Router = require("koa-router");
const strategyRoute = new Router();
const koaBody = require("koa-body");
const { driverController } = require("../../controller");

strategyRoute.post("/set", driverController.postStrategy);
strategyRoute.get("/", driverController.getStrategy);

module.exports = strategyRoute;

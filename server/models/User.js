"use strict";
const bcrypt = require("bcryptjs");
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

/**
 * @class UserSchema
 * @memberOf module:FModel
 * @constructor
 * @mixes Document
 * @property {String} password 密码 默认值为111111
 * @property {String} account 唯一用户名
 * @property {Boolean} isAdmin 是否为管理员
 * @property {Date} createTime 创建时间
 */
const UserSchema = new Schema({
  account: { type: String, required: true },
  password: { type: String, default: bcrypt.hashSync("111111", 5) },
  email: { type: String, required: true },
  isAdmin: { type: Boolean, required: true, default: true },
  createTime: { type: Date, required: true, default: Date.now }
});

UserSchema.index({ account: 1 }, { unique: true });

mongoose.model("User", UserSchema);

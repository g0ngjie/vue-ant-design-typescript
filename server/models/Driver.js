"use strict";

const mongoose = require("mongoose");
const Schema = mongoose.Schema;

/**
 * 本表用来存储驱动版本信息
 * @calss DriverSchema
 * @memberof module:FModel
 * @constructor
 * @mixes Document
 * @property {Date} createTime 驱动上传时间
 * @property {Number|FEnum.OsType} osType 系统类型
 * @property {Number|FEnum.DriverType} driverType 驱动类型
 * @property {Number|FEnum.CpuBits} cpuBits 系统位数
 * @property {String} fileName 驱动名称
 * @property {Number} fileSize 驱动大小
 * @property {String} version 版本类型
 * @property {String} description 变更履历
 * @property {Boolean} isNewest 是否是最新版本
 * @property {String} httpPath 驱动静态资源的映射地址
 * @property {String} md5 md5文件加密標示
 * @property {String} filePath 文件上传路径
 * @property {Number|FEnum.InstallMethod} method 安装方式
 * @property {Number} 查询的间隔时间,默认10min
 */

const DriverSchema = new Schema({
  createTime: { type: Date, required: true, default: Date.now },
  osType: { type: Number, required: true },
  driverType: { type: Number, required: true },
  cpuBits: { type: Number, required: true },
  fileName: { type: String },
  fileSize: { type: Number, required: true },
  version: { type: String, required: true },
  description: { type: String },
  isNewest: { type: Boolean, default: false },
  httpPath: { type: String },
  md5: { type: String },
  filePath: { type: String, required: true }
});

DriverSchema.index(
  { osType: 1, driverType: 1, version: 1, cpuBits: 1 },
  { unique: 1 }
);

mongoose.model("Driver", DriverSchema);

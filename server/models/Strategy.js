"use strict";
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const FEnum = require("../util/FEnum");

/**
 * @class StrategySchema
 * @memberOf module:FModel
 * @constructor
 * @mixes Document
 * @property {Number} method 密码 安装方式
 * @property {Number} timeInterval 间隔时间
 */
const StrategySchema = new Schema({
  method: { type: Number, default: FEnum.InstallMethod.m0 },
  timeInterval: { type: Number, default: 600 }
});

mongoose.model("Strategy", StrategySchema);

"use strict";
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const FEnum = require("../util/FEnum");

/**
 * @class InstallLogSchema
 * @memberOf module:FModel
 * @constructor
 * @mixes Document
 * @property {Date} createTime 驱动安装时间
 * @property {String} userName pc端用户名
 * @property {String} hostName pc端主机名
 * @property {String} domainName pc端域名
 * @property {String} version 升级包的版本号
 * @property {Number|FEnum.DriverType} driverType 驱动类型
 * @property {Number|FEnum.OsType} osType 系统类型
 * @property {Number|FEnum.CpuBits} cpuBits 位数
 * @property {String} osVer 当前操作系统版本
 * @property {String} oldVer 升级前的版本
 * @property {String} newVer 升级后的版本
 * @property {Number|FEnum.InstallResult} result 安装结果
 * @property {String} httpPath 失败文件存储地址(静态资源)
 */
const InstallLogSchema = new Schema({
  createTime: { type: Date, required: true, default: Date.now },
  userName: { type: String, required: true },
  hostName: { type: String },
  domainName: { type: String },
  version: { type: String },
  driverType: { type: Number, required: true },
  osType: { type: Number, required: true },
  cpuBits: { type: Number, required: true },
  osVer: { type: String },
  oldVer: { type: String, required: true },
  newVer: { type: String, required: true },
  result: {
    type: Number,
    required: true,
    default: FEnum.InstallResult.success
  },
  httpPath: { type: String }
});

mongoose.model("InstallLog", InstallLogSchema);

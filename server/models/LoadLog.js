"use strict";
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

/**
 * @class LoadLogSchema
 * @memberOf module:FModel
 * @constructor
 * @mixes Document
 */
const LoadLogSchema = new Schema({
  userName: { type: String, required: true },
  hostName: { type: String },
  version: { type: String },
  driverType: { type: Number, required: true },
  osType: { type: Number, required: true },
  cpuBits: { type: Number, required: true },
  osVer: { type: String },
  errNo: { type: String },
  msg: { type: String },
  downTime: { type: Date, required: true, default: Date.now }
});

mongoose.model("LoadLog", LoadLogSchema);

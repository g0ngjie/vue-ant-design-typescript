"use strict";

/**
 * 根据模板生成config文件
 */

const fs = require("fs");
const path = require("path");
const FUtil = require("../util/FUtil");
const format = require("string-format");
format.extend(String.prototype);

const template = `module.exports = {
  version: "v0.1.0",
  domain: {
    host: "{host}",
    port: {port}
  },
  mongo: "mongodb://{host}:27017/hood_driver",
  authToken: "5dc90e7631059c00080f7005",
  aesSecret: "41b76a42-be3b-4805-abdc-2f63e8442ced",
  redis: {
    port: 6379,
    host: "{host}",
    db: 4
  }
};
`;

function startup(port) {
  let localIp = FUtil.getLocalIP();
  const conPath = path.join(__dirname, "../config");
  if (fs.existsSync(conPath + "/config.js")) {
    fs.unlinkSync(conPath + "/config.js");
  }
  localIp = localIp.split(";").length > 0 ? localIp.split(";")[0] : "127.0.0.1";
  const contents = template.format({ host: localIp, port: port || 8081 });
  fs.writeFileSync(conPath + "/config.js", contents);
}

startup();

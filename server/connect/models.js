"use strict";

const mongoose = require("mongoose");
const config = require("../config/config");
mongoose.set("useUnifiedTopology", true);

/**连接数据库 */
mongoose.connect(
  config.mongo,
  {
    poolSize: 20,
    useCreateIndex: true,
    useNewUrlParser: true,
    useFindAndModify: false
  },
  function(err) {
    if (err) {
      process.exit(1);
    }
  }
);

require("../models/Driver");
require("../models/InstallLog");
require("../models/LoadLog");
require("../models/User");
require("../models/Strategy");

/**实例化 */
exports.Driver = mongoose.model("Driver");
exports.InstallLog = mongoose.model("InstallLog");
exports.LoadLog = mongoose.model("LoadLog");
exports.User = mongoose.model("User");
exports.Strategy = mongoose.model("Strategy");

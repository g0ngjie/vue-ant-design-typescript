"use strict";

const config = require("../config/config");
const Redis = require("ioredis");
const redisConfig = config.redis;

const client = new Redis({
  port: redisConfig.port || 6379,
  host: redisConfig.host || "localhost",
  db: redisConfig.db || 0,
  password: redisConfig.password || ""
});

client.on("error", function(err) {
  if (err) {
    process.exit(1);
  }
});

exports = module.exports = client;

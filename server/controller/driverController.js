"use strict";

const path = require("path");
const fs = require("fs");
const { Driver } = require("../schemas");
const { FLayout, FUtil } = require("../util");
const FError = require("../util/FError");
const { driverProxy } = require("../proxy");
let uploadPath = path.resolve(__dirname, "../hubs");

// exports.uploadDriver = async ctx => {
//   const { path: tempPath } = ctx.request.files.file;
//   const { name, fileName } = ctx.request.body;
//   const chunksPath = path.join(uploadPath, `/${fileName}_dir/`);
//   FUtil.streamTranser({
//     path: tempPath,
//     name: chunksPath + name
//   });
//   FLayout.layout(null, ctx);
// };

exports.uploadHugeDriver = async ctx => {
  const { path: tempPath, name, size: fSize } = ctx.request.files.file;
  FUtil.streamTranser({
    path: tempPath,
    name: uploadPath + "/" + name
  });
  FLayout.layout(null, ctx, { filePath: uploadPath + "/" + name, fileSize: fSize });
};

exports.checkFinish = async ctx => {
  const { filePath, fileSize } = ctx.request.body;
  const exist = fs.existsSync(filePath);
  if (exist) {
    const stat = fs.statSync(filePath);
    const { size } = stat;
    if ("" + size === "" + fileSize) {
      FLayout.layout(null, ctx, { errNo: 0, msg: "finished" });
    } else {
      FLayout.layout(null, ctx, { errNo: -1, msg: "not finished" });
    }
  } else {
    FLayout.layout(null, ctx, { errNo: -1, msg: "not finished" });
  }
};

exports.mergeDriver = async ctx => {
  const req = ctx.request;
  const reqBody = FUtil.reqVerify(req.body, Driver.mergeDriver);
  if (!reqBody) ctx.throw(400, FError.InvalidParameters("invalid parameters"));
  const info = await driverProxy.mergeDriver(reqBody, uploadPath);
  FLayout.layout(null, ctx, info);
};

exports.getInstallLogs = async ctx => {
  const req = ctx.request;
  const reqBody = FUtil.reqVerify(req.body, Driver.InstallLogSchema);
  if (!reqBody) ctx.throw(400, FError.InvalidParameters("invalid parameters"));
  const logs = await driverProxy.searchInstallLogs(reqBody);
  FLayout.layout(null, ctx, logs);
};

exports.countInstallLogs = async ctx => {
  const req = ctx.request;
  const reqBody = FUtil.reqVerify(req.body, Driver.InstallLogSchema);
  if (!reqBody) ctx.throw(400, FError.InvalidParameters("invalid parameters"));
  const results = await driverProxy.countInstallLogs(reqBody);
  FLayout.layout(null, ctx, results);
};

exports.getDrivers = async ctx => {
  const req = ctx.request;
  const reqBody = FUtil.reqVerify(req.body, Driver.DriverSchema);
  if (!reqBody) ctx.throw(400, FError.InvalidParameters("invalid parameters"));
  const drivers = await driverProxy.searchDrivers(reqBody);
  FLayout.layout(null, ctx, drivers);
};

exports.countDrivers = async ctx => {
  const req = ctx.request;
  const reqBody = FUtil.reqVerify(req.body, Driver.DriverSchema);
  if (!reqBody) ctx.throw(400, FError.InvalidParameters("invalid parameters"));
  const results = await driverProxy.countDrivers(reqBody);
  FLayout.layout(null, ctx, results);
};

exports.removeDriver = async ctx => {
  const did = ctx.params["did"];
  await driverProxy.removeDriver(did);
  FLayout.layout(null, ctx);
};

exports.setDriver = async ctx => {
  const req = ctx.request;
  const reqBody = FUtil.reqVerify(req.body, Driver.DrivenSchema);
  if (!reqBody) ctx.throw(400, FError.InvalidParameters("invalid parameters"));
  await driverProxy.drivenSet(reqBody);
  FLayout.layout(null, ctx);
};

exports.updateDriver = async ctx => {
  const did = ctx.params["did"];
  const req = ctx.request;
  const reqBody = FUtil.reqVerify(req.body, Driver.DrivenSchema);
  if (!reqBody) ctx.throw(400, FError.InvalidParameters("invalid parameters"));
  await driverProxy.updateDriverSet(did, reqBody);
  FLayout.layout(null, ctx);
};

exports.getVersions = async ctx => {
  const versions = await driverProxy.acquireVersions();
  FLayout.layout(null, ctx, versions);
};

exports.postStrategy = async ctx => {
  const req = ctx.request;
  const reqBody = FUtil.reqVerify(req.body, Driver.StrategySchema);
  if (!reqBody) ctx.throw(400, FError.InvalidParameters("invalid parameters"));
  await driverProxy.setStrategy(reqBody);
  FLayout.layout(null, ctx);
};

exports.getStrategy = async ctx => {
  const strategy = await driverProxy.getStrategy();
  FLayout.layout(null, ctx, { strategy });
};

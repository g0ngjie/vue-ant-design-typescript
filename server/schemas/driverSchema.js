"use strict";

const Joi = require("joi");
const FEnum = require("../util/FEnum");

exports.mergeDriver = Joi.object({
  total: Joi.number().required(),
  fname: Joi.string().required(),
  fsize: Joi.number().required()
});

exports.upgPack = Joi.object({
  authToken: Joi.string().required(),
  hostName: Joi.string().required(),
  userName: Joi.string().required(),
  domainName: Joi.string().required(),
  osVer: Joi.string().required(),
  curPackVer: Joi.string(),
  version: Joi.string(),
  downTime: Joi.string(),
  result: Joi.string(),
  oldVer: Joi.string(),
  newVer: Joi.string(),
  errNo: Joi.string(),
  msg: Joi.string(),
  driverType: Joi.string()
    .regex(/^[0-9]$/)
    .required(),
  cpuBits: Joi.string()
    .regex(/^[0-9]$/)
    .required(),
  osType: Joi.string()
    .regex(/^[0-9]$/)
    .required()
});

/**安装日志查询 */
exports.InstallLogSchema = Joi.object({
  hostName: Joi.string(),
  userName: Joi.string(),
  driverType: Joi.number(),
  osType: Joi.number(),
  result: Joi.number(),
  startTime: Joi.string(),
  endTime: Joi.string(),
  page: Joi.number(),
  count: Joi.number(),
  sortBy: Joi.number().valid(Object.values(FEnum.SortBy))
});

/**驱动查询 */
exports.DriverSchema = Joi.object({
  driverType: Joi.number(),
  osType: Joi.number(),
  result: Joi.number(),
  page: Joi.number(),
  count: Joi.number(),
  isNewest: Joi.boolean(),
  sortBy: Joi.number().valid(Object.values(FEnum.SortBy))
});

/**驱动设置 */
exports.DrivenSchema = Joi.object({
  fileName: Joi.string() /**驱动名称 */,
  driverType: Joi.number().required(),
  cpuBits: Joi.number().default(FEnum.CpuBits.general),
  osType: Joi.number().required(),
  version: Joi.string().required(),
  description: Joi.string(),
  filePath: Joi.string().required(),
  fileSize: Joi.number().required()
});

/**策略设置 */
exports.StrategySchema = Joi.object({
  method: Joi.number().default(FEnum.InstallMethod.m0),
  timeInterval: Joi.number().default(600) /**十分钟 */
});

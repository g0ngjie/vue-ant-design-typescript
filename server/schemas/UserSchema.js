"use strict";

const Joi = require("joi");

exports.postAdmin = Joi.object({
  account: Joi.string().required(),
  password: Joi.string()
});

exports.updatePass = Joi.object({
  account: Joi.string().required(),
  originPass: Joi.string().required(),
  newPass: Joi.string().required(),
  repeatPass: Joi.string().required()
});

"use strict";
const winston = require("winston");
const FUtil = require("./FUtil");

exports.generateErrorLog = async (statusCode, error, ctx) => {
  if (!error) {
    return true;
  }
  const [url, query] = (ctx.originalUrl || "").split("?");
  return {
    statusCode,
    url,
    query,
    requestBody: JSON.stringify(ctx.response.body || ""),
    method: ctx.request.method,
    requestIP: FUtil.getRemoteIP(ctx.request),
    responseIP: FUtil.getLocalIP(),
    responseError: "" + error,
    createTime: new Date()
  };
};

const logger = winston.createLogger({
  level: "info",
  format: winston.format.json(),
  defaultMeta: { service: "doc-service" },
  transports: [
    new winston.transports.Console({ level: "error", name: "error-msg" }),
    new winston.transports.Console({ level: "info", name: "info-msg" })
  ]
});

/**抛错提示 */
exports.loggerError = async error => {
  const { errorMsg } = error;
  logger.level = "debug";
  return await logger.log("error", errorMsg);
};

/**打印提示 */
exports.loggerInfo = async info => {
  return await logger.log("info", info);
};
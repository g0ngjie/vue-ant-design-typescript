"use strict";

/**
 * 驱动类型
 * enum for DriverType
 * @readonly
 * @enum {Number}
 */
exports.DriverType = {
  direct: 0 /**漫游 */,
  roaming: 1 /**直达加漫游 */,
  other: 2 /**其他 */
};

/**
 * 操作系统位数
 * enum for CpuBits
 * @readonly
 * @enum {Number}
 */
exports.CpuBits = {
  win64: 0 /**64位系统 */,
  win32: 1 /**32位系统 */,
  general: 2 /**通用 */
};

/**
 * 操作系统类型
 * enum for OsType
 * @readonly
 * @enum {Number}
 */
exports.OsType = {
  win: 0,
  mac: 1,
  android: 2
};

/**
 * 项目中需要用到的缓存key
 * enum for RedisKey
 * @readonly
 * @enum {String}
 */
exports.RedisKey = {
  tokenKey: "upgpack:token:{uid}" /**token缓存 */,
  loadQueue: "upgpack:load:queue" /**下载驱动包队列 */
};

/**
 * 全局的数据
 * enum for MaxLimit
 * @readonly
 * @enum {Number}
 */
exports.MaxLimit = {
  LoaderExipreTime: 60 * 1000 /**下载者队列缓存时间 1min */,
  TokenExpireTime: 120 * 60 /**一次登录过期时间 2h */,
  LimitLoad: 50 /**下载驱动队列中承压的最大人数 50人 */
};

/**
 * 排序枚举
 * enum for SortBy
 * @readonly
 * @enum {Number}
 */
exports.SortBy = {
  asc: 1,
  desc: -1
};

/**
 * 安装方式
 * enum for InstallMethod
 * @readonly
 * @enum {Number}
 */
exports.InstallMethod = {
  m0: 0,
  m1: 1,
  m2: 2
};

/**
 * 安装结果
 * enum for InstallResult
 * @readonly
 * @enum {Number}
 */
exports.InstallResult = {
  success: 1,
  failure: 0
};

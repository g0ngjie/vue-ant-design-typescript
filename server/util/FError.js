"use strict";

class FError extends Error {
  constructor(errorCode, errorMsg, statusCode) {
    super(errorMsg, statusCode);
    this.errorCode = errorCode;
    this.errorMsg = errorMsg;
    this.statusCode = statusCode;
  }

  static ErrorMap(error) {
    return {
      [FError.RequestError]: 1000,
      [FError.InvalidParameters]: 2001,
      [FError.InvalidAdmin]: 2002,
      [FError.NotFoundDept]: 2003,
      [FError.DeptDelNotAllowed]: 2004,
      [FError.NotFoundUser]: 2005,
      [FError.UserHasExist]: 2006,
      [FError.VerifyFailure]: 2007,
      [FError.DuplicateKey]: 2008,
      [FError.NotFoundRule]: 2009,
      [FError.VerifyPathFailure]: 2010,
      [FError.AuthenticationRole]: 2011,
      [FError.EmailHasUsed]: 2012,
      [FError.InvalidLogin]: 2013,
      [FError.ConnectRefused]: 2014,
      [FError.DocumentNotFound]: 2015,
      [FError.SystemSetNotFinished]: 2016,
      [FError.TokenExpired]: 2017,
      [FError.InvalidFormatData]: 2018,
      [FError.DocAuditNotAllowed]: 2019,
      [FError.DocAcceptNotAllowed]: 2020,
      [FError.DocSealNotAllowed]: 2021,
      [FError.DocArchiveNotAllowed]: 2022,
      [FError.DocNotOptAccess]: 2023,
      [FError.ConnectTimeOut]: 2024,
      [FError.SameDeptWithParent]: 2025,
      [FError.NotEmailFound]: 2026,
      [FError.ExistInvalidPatr]: 2027,
      [FError.NoCompanyFound]: 2028,
      [FError.InvalidSealDept]: 2029,
      [FError.InvalidAddRoot]: 2030,
      [FError.ExistTwoBscCenter]: 2031,
      [FError.InvalidUpdateRoot]: 2032,
      [FError.BscRoleValidateError]: 2033,
      [FError.VerificationExpired]: 2034,
      [FError.NotFoundBscCenter]: 2035,
      [FError.NotFoundSealer]: 2036,
      [FError.NotFoundBscLeader]: 2037,
      [FError.UserDelNotAllowed]: 2038,
      [FError.InvalidSealDeptTrans]: 2039,
      [FError.InvalidEncoding]: 2040,
      [FError.DuplicateRule]: 2041,
      [FError.LoginTwice]: 2042,
      [FError.NotQRCodeFound]: 2043,
      [FError.ErrorParseQRCode]: 2044,
      [FError.NotFoundSys]: 2045,
      [FError.AccountHasUsed]: 2051,
      [FError.ExistTwoLeader]: 2052,
      [FError.NotFoundArchiver]: 2053,
      [FError.NotFoundSealRule]: 2054,
      [FError.DuplicateSealRule]: 2055,
      [FError.ResetNotAllowed]: 2057,
      /**驱动检测系统 */
      [FError.InvalidFileNum]: 3000,
      [FError.InvalidFileStat]: 3001,
      [FError.NotDriverFound]: 3002,
      [FError.ExistInQueue]: 3003,
      [FError.UserOverLoaded]: 3004
    }[error];
  }

  /**
   * 以下定义为错误枚举类型
   * @function isTypeOf
   * @para  {*} error
   * @param {*} ErrorType
   */
  static isTypeOf(error, ErrorType) {
    if (!error) return false;
    return error.errorCode === FError.ErrorMap(ErrorType);
  }

  /**
   * 系统错误
   */
  static RequestError(errorMsg, statusCode) {
    return new FError(1000, errorMsg || "internal error", statusCode);
  }

  /**
   * 非法参数
   */
  static InvalidParameters(errorMsg, statusCode) {
    return new FError(2001, errorMsg || "invalid parameters", statusCode);
  }

  /**
   * 没有管理员权限
   */
  static InvalidAdmin(errorMsg, statusCode) {
    return new FError(2002, errorMsg, statusCode);
  }

  /**
   * 未查找到部门
   */
  static NotFoundDept(errorMsg, statusCode) {
    return new FError(2003, errorMsg, statusCode);
  }

  /**
   * 该部门可能为其他部门的父部门，不允许被删除
   */
  static DeptDelNotAllowed(errorMsg, statusCode) {
    return new FError(2004, errorMsg, statusCode);
  }

  /**
   * 未查找到用户
   */
  static NotFoundUser(errorMsg, statusCode) {
    return new FError(2005, errorMsg, statusCode);
  }

  /**
   * 用户已存在
   */
  static UserHasExist(errorMsg, statusCode) {
    return new FError(2006, errorMsg, statusCode);
  }

  /**
   * 验证失败
   */
  static VerifyFailure(errorMsg, statusCode) {
    return new FError(2007, errorMsg, statusCode);
  }

  /**
   * 重复索引报错
   */
  static DuplicateKey(errorMsg, statusCode) {
    return new FError(2008, errorMsg, statusCode);
  }

  /**
   * 未查找到规则
   */
  static NotFoundRule(errorMsg, statusCode) {
    return new FError(2009, errorMsg, statusCode);
  }

  /**
   * 验证审批路径失败
   */
  static VerifyPathFailure(errorMsg, statusCode) {
    return new FError(2010, errorMsg, statusCode);
  }

  /**
   * 角色和权限不匹配
   */
  static AuthenticationRole(errorMsg, statusCode) {
    return new FError(2011, errorMsg, statusCode);
  }

  /**
   * 邮箱已经被占用
   */
  static EmailHasUsed(errorMsg, statusCode) {
    return new FError(2012, errorMsg, statusCode);
  }

  /**
   * 账号或密码错误
   */
  static InvalidLogin(errorMsg, statusCode) {
    return new FError(2013, errorMsg, statusCode);
  }

  /**
   * 错误的连接地址,连接被拒绝
   */
  static ConnectRefused(errorMsg, statusCode) {
    return new FError(2014, errorMsg, statusCode);
  }

  /**
   * 文档不存在
   */
  static DocumentNotFound(errorMsg, statusCode) {
    return new FError(2015, errorMsg, statusCode);
  }

  /**
   * 系统设定需要重新设置
   */
  static SystemSetNotFinished(errorMsg, statusCode) {
    return new FError(2016, errorMsg, statusCode);
  }

  /**
   * token过期
   */
  static TokenExpired(errorMsg, statusCode) {
    return new FError(2017, errorMsg, statusCode);
  }

  /**
   * 数据格式不正确
   */
  static InvalidFormatData(errorMsg, statusCode) {
    return new FError(2018, errorMsg, statusCode);
  }

  /**
   * 不允许提交审查
   */
  static DocAuditNotAllowed(errorMsg, statusCode) {
    return new FError(2019, errorMsg, statusCode);
  }

  /**
   * 不允许提交盖章审批
   */
  static DocAcceptNotAllowed(errorMsg, statusCode) {
    return new FError(2020, errorMsg, statusCode);
  }
  /**
   * 不允许提交盖章
   */
  static DocSealNotAllowed(errorMsg, statusCode) {
    return new FError(2021, errorMsg, statusCode);
  }

  /**
   * 不允许提交存档
   */
  static DocArchiveNotAllowed(errorMsg, statusCode) {
    return new FError(2022, errorMsg, statusCode);
  }

  /**
   * 没有对文档的操作权限
   */
  static DocNotOptAccess(errorMsg, statusCode) {
    return new FError(2023, errorMsg, statusCode);
  }

  /**
   * 请求超时
   */
  static ConnectTimeOut(errorMsg, statusCode) {
    return new FError(2024, errorMsg, statusCode);
  }

  /**
   * 不允许选取自己为自己的父部门
   */
  static SameDeptWithParent(errorMsg, statusCode) {
    return new FError(2025, errorMsg, statusCode);
  }

  /**
   * 用户邮箱未设置
   */
  static NotEmailFound(errorMsg, statusCode) {
    return new FError(2026, errorMsg, statusCode);
  }

  /**
   * 导入部门时存在既匹配不上本地也匹配不上数据库中数据的父部门
   */
  static ExistInvalidPatr(errorMsg, statusCode) {
    return new FError(2027, errorMsg, statusCode);
  }

  /**
   * 未创建根部门
   */
  static NoCompanyFound(errorMsg, statusCode) {
    return new FError(2028, errorMsg, statusCode);
  }

  /**
   * 创建非法的bsc部门(其父部门不是bsc部门)
   */
  static InvalidSealDept(errorMsg, statusCode) {
    return new FError(2029, errorMsg, statusCode);
  }

  /**
   * 非法创建根部门
   */
  static InvalidAddRoot(errorMsg, statusCode) {
    return new FError(2030, errorMsg, statusCode);
  }

  /**
   * csv中存在两个bsc中心
   */
  static ExistTwoBscCenter(errorMsg, statusCode) {
    return new FError(2031, errorMsg, statusCode);
  }

  /**
   * 非法更新根部门
   */
  static InvalidUpdateRoot(errorMsg, statusCode) {
    return new FError(2032, errorMsg, statusCode);
  }

  /**
   * 盖章担当、存档担当必须为bsc部门员工
   */
  static BscRoleValidateError(errorMsg, statusCode) {
    return new FError(2033, errorMsg, statusCode);
  }

  /**
   * 验证码过期
   */
  static VerificationExpired(errorMsg, statusCode) {
    return new FError(2034, errorMsg, statusCode);
  }

  /**
   * 未查找到bsc中心及其子部门，无法发起盖章流程
   */
  static NotFoundBscCenter(errorMsg, statusCode) {
    return new FError(2035, errorMsg, statusCode);
  }

  /**
   * 未查找到盖章担当
   */
  static NotFoundSealer(errorMsg, statusCode) {
    return new FError(2036, errorMsg, statusCode);
  }

  /**
   * 某个层级的bsc领导未设置
   */
  static NotFoundBscLeader(errorMsg, statusCode) {
    return new FError(2037, errorMsg, statusCode);
  }

  /**
   * 用户在审批路径中，不允许删除
   */
  static UserDelNotAllowed(errorMsg, statusCode) {
    return new FError(2038, errorMsg, statusCode);
  }

  /**
   * bsc部门不允许更改为非bsc部门
   */
  static InvalidSealDeptTrans(errorMsg, statusCode) {
    return new FError(2039, errorMsg, statusCode);
  }

  /**
   * 文件格式不合法
   */
  static InvalidEncoding(errorMsg, statusCode) {
    return new FError(2040, errorMsg, statusCode);
  }

  /**
   * 规则中的文档类型重复
   */
  static DuplicateRule(errorMsg, statusCode) {
    return new FError(2041, errorMsg, statusCode);
  }

  /*
   * 异地登录
   */
  static LoginTwice(errorMsg, statusCode) {
    return new FError(2042, errorMsg, statusCode);
  }

  /**
   * 未检测到二维码
   */
  static NotQRCodeFound(errorMsg, statusCode) {
    return new FError(2043, errorMsg, statusCode);
  }

  /**
   * 二维码解析错误
   */
  static ErrorParseQRCode(errorMsg, statusCode) {
    return new FError(2044, errorMsg, statusCode);
  }

  /**
   * 未查到系统配置
   */
  static NotFoundSys(errorMsg, statusCode) {
    return new FError(2045, errorMsg, statusCode);
  }

  /*
   * 该账号已经被占用
   */
  static AccountHasUsed(errorMsg, statusCode) {
    return new FError(2051, errorMsg, statusCode);
  }

  /**
   * 存在两个领导在一个部门里
   */
  static ExistTwoLeader(errorMsg, statusCode) {
    return new FError(2052, errorMsg, statusCode);
  }

  /**
   * 未查找到存档担当
   */
  static NotFoundArchiver(errorMsg, statusCode) {
    return new FError(2053, errorMsg, statusCode);
  }

  /**
   * 未查找到用印规则
   */
  static NotFoundSealRule(errorMsg, statusCode) {
    return new FError(2054, errorMsg, statusCode);
  }

  /**
   * 用印规则名或路径重复
   */
  static DuplicateSealRule(errorMsg, statusCode) {
    return new FError(2055, errorMsg, statusCode);
  }

  /**
   * 重置操作不被允许
   */
  static ResetNotAllowed(errorMsg, statusCode) {
    return new FError(2057, errorMsg, statusCode);
  }

  /**
   * 文件数目不对
   */
  static InvalidFileNum(errorMsg, statusCode) {
    return new FError(3000, errorMsg, statusCode);
  }

  /**
   * 文件状态不对
   */
  static InvalidFileStat(errorMsg, statusCode) {
    return new FError(3001, errorMsg, statusCode);
  }

  /**
   * 沒有發現驅動
   */
  static NotDriverFound(errorMsg, statusCode) {
    return new FError(3002, errorMsg, statusCode);
  }

  /**
   * 已经在下载队列中，避免短时间内重复下载
   */
  static ExistInQueue(errorMsg, statusCode) {
    return new FError(3003, errorMsg, statusCode);
  }

  /**
   * 下载人数过多，拥挤
   */
  static UserOverLoaded(errorMsg, statusCode) {
    return new FError(3004, errorMsg, statusCode);
  }
}

module.exports = FError;

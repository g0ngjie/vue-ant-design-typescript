"use strict";
const FError = require("../util/FError");
const { FUtil, FEnum, FLayout } = require("../util");
const { Driver } = require("../schemas");
const config = require("../config/config");

exports.validateupgPack = async (ctx, next) => {
  const reqBody = ctx.request.body;
  const { success, failure } = FEnum.InstallResult;
  const validate = FUtil.reqVerify(reqBody, Driver.upgPack);
  if (!validate)
    return FLayout.layout(null, ctx, {
      errNo: 2001,
      msg: "invalid parameters"
    });
  const { authToken } = validate;
  if (authToken !== config.authToken)
    return FLayout.layout(null, ctx, { errNo: 2001, msg: "wrong authToken" });
  /**客户端的body参数中的数据类型不好处理(全都是string类型)，以后开发过程中由后端在此处用中间件进行格式转换 */
  if (reqBody.driverType) reqBody.driverType = +reqBody.driverType;
  if (reqBody.cpuBits) reqBody.cpuBits = +reqBody.cpuBits;
  if (reqBody.osType) reqBody.osType = +reqBody.osType;
  if (reqBody.oldVer && reqBody.oldVer === "0.0.0.0") reqBody.oldVer = "";
  if (reqBody.result) {
    const { result } = reqBody;
    reqBody.result = result === "1" || result === success ? success : failure;
  }
  ctx.reqBody = reqBody;
  await next();
};

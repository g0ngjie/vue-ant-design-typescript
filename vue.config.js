const { DevServer } = require("./dev.config.js");

module.exports = {
  // 基本路径
  publicPath: "./",
  // 输出文件目录
  outputDir: "dist",
  // eslint-loader 是否在保存的时候检查
  lintOnSave: true,
  // use the full build with in-browser compiler?
  // compiler: false,
  // webpack配置
  chainWebpack: () => {},
  configureWebpack: () => {},
  // vue-loader 配置项
  // vueLoader: {},
  // 生产环境是否生成 sourceMap 文件git@gitlab-spd2k:spd2k/rits_driver.gitgit@gitlab-spd2k:spd2k/rits_driver.git
  productionSourceMap: false,
  // css相关配置
  css: {
    extract: true,
    // 开启 CSS source maps?
    sourceMap: false,
    // css预设器配置项
    loaderOptions: {
      less: {
        modifyVars: {
          "menu-dark-bg": "#2F4056"
          // "primary-color": "#1DA57A",
          // "link-color": "#1DA57A",
          // "border-radius-base": "2px",
          // "success-color": "red"
        },
        javascriptEnabled: true
      }
    },
    // 启用 CSS modules for all css / pre-processor files.
    requireModuleExtension: true
  },
  // use thread-loader for babel & TS in production build
  // enabled by default if the machine has more than 1 cores
  parallel: require("os").cpus().length > 1,
  // 是否启用dll
  // dll: false,
  // webpack-dev-server 相关配置
  devServer: DevServer,
  // 第三方插件配置
  pluginOptions: {
    // ...
  }
};

/**
 * proxy
 */
const DevConf = {
  Local: "http://127.0.0.1:3000",
  Remote: "http://172.25.73.17:50001",
  Soho: "http://192.168.1.103:3000"
};

const target = DevConf.Soho;

exports.DevServer = {
  host: "localhost",
  port: 8080,
  https: false,
  hot: true, // 开启热模块加载
  proxy: {
    "/api": {
      target,
      changeOrigin: true,
      pathRewrite: {
        "^/api": "/"
      }
    }
  }
};

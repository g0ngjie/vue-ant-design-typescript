/**
 * router path
 */
export enum RouterPath {
  REDIRECT = "/redirect" /* 重定向 */,
  LOGIN = "/login" /* 登录 */,
  HOME = "/" /* 首页 */,
  DRIVE = "/drive" /* 驱动管理 */,
  //   Windows = "/driver/windows" /* window 驱动 */,
  //   MacOS = "/driver/macos" /* macos 驱动 */,
  LOGS = "/logs" /* 日志 */,
  // PassWd = "/passwd" /* 修改密码 */
  INIT_ADMIN = "/init",
  STRATEGY = "/strategy" /* 升级策略 */
}

/**
 * localStorage
 */
export enum LocalStorage {
  TOKEN,
  USER
}

/**
 * Sidebar:
 *  - defaultOpenkeys
 *  - defaultSelectKeys
 */
export enum SidebarDefault {
  OPEN_KEY = RouterPath.DRIVE,
  SELECT_KEY = RouterPath.DRIVE
}

/**
 * 驱动类型
 * enum for DriverType
 * @readonly
 * @enum {Number}
 */
export enum DriverType {
  ROAMING /**云打印驱动 */,
  DIRECT /**直接打印驱动*/,
  OTHER /**其他 */
}

/**
 * 操作系统位数
 * enum for CpuBits
 * @readonly
 * @enum {Number}
 */
export enum CpuBits {
  WIN64 /**64位系统 */,
  WIN32 /**32位系统 */,
  GENERAL
}

/**
 * 操作系统类型
 * enum for OsType
 * @readonly
 * @enum {Number}
 */
export enum OsType {
  WIN,
  MAC,
  ANDROID
}

/**
 * 升级结果
 *
 * @export
 * @enum {number}
 */
export enum UpgradeResult {
  ERROR,
  SUCCESS
}

/**
 * 开始 & 结束 日期枚举
 *
 * @export
 * @enum {number}
 */
export enum DateRound {
  START = 1,
  END
}

/**
 * 安装方式
 * enum for InstallMethod
 * @readonly
 * @enum {Number}
 */
export enum InstallMethod {
  M0,
  M1,
  M2
}

/**
 * 排序
 * @readonly
 * @enum {Number}
 */
export enum SortBy {
  ASC = 1,
  DESC = -1
}

/**
 * 上传文件结果
 *
 * @export
 * @enum {number}
 */
export enum UploadFinish {
  SUCCESS,
  ERROR = -1
}
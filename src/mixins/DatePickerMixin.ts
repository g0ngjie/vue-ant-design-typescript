import Vue from "vue";
import Component from "vue-class-component";

@Component({ name: "DatePickerMixin" })
export default class DatePickerMixin extends Vue {
  startValue: any = null;
  endValue: any = null;

  disabledStartDate(startValue: any) {
    const endValue = this.endValue;
    if (!startValue || !endValue) {
      return false;
    }
    return startValue.valueOf() > endValue.valueOf();
  }
  disabledEndDate(endValue: any) {
    const startValue = this.startValue;
    if (!endValue || !startValue) {
      return false;
    }
    return startValue.valueOf() >= endValue.valueOf();
  }
}

import Vue from "vue";
import Component from "vue-class-component";

@Component({ name: "PaginationMixin" })
export default class PaginationMixin extends Vue {
  pageSize: number = 10;

  pagination: any = {
    current: 1,
    total: 0,
    defaultPageSize: 10,
    showSizeChanger: true,
    onShowSizeChange: (current: any, pageSize: any) => {
      this.setPageSize(pageSize);
    }
  };

  setPageSize(size: number) {
    this.pageSize = size;
  }
}

import Vue from "vue";
//解决vue在ie的兼容性问题
import "babel-polyfill";
import App from "./App.vue";
import router from "./router";
import store from "./store";
// global styles
import "@/styles/index.less";
// import "@/assets/icon/iconfont.css";
// vue-antdesign 按需加载
import "@/common/frame";
// interceptor
import "@/common/interceptor";

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");

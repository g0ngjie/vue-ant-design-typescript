import moment from "moment";

/**
 * 格式化日期
 * @param {*} date
 */
export function fmtDate(date: any) {
  return date && moment(date).format("YYYY-MM-DD HH:mm:ss");
}

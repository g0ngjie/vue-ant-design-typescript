import Vue from "vue";
import VueRouter from "vue-router";
import Layout from "@/layout/index.vue";
import { RouterPath as PathEnum } from '@/utils/VEnum'

Vue.use(VueRouter);

export const Routes: any[] = [
  {
    path: '/redirect',
    component: Layout,
    meta: { hidden: true },
    children: [
      {
        path: '/redirect/:path*',
        component: () => import(/* webpackChunkName: "redirect" */ '@/views/redirect/index.vue')
      }
    ]
  },
  {
    path: PathEnum.LOGIN,
    meta: { hidden: true },
    component: () => import('@/views/login/index.vue')
  },
  {
    path: PathEnum.HOME,
    meta: { hidden: true },
    redirect: PathEnum.DRIVE
  },
  {
    path: PathEnum.DRIVE,
    component: Layout,
    meta: { icon: 'tool', title: '驱动管理' },
    children: [
      {
        path: PathEnum.DRIVE,
        component: () => import("@/views/drive/index.vue")
      }
    ]
  },
  {
    path: PathEnum.LOGS,
    component: Layout,
    meta: { icon: 'file-text', title: '日志管理' },
    children: [
      {
        path: PathEnum.LOGS,
        component: () => import("@/views/logs/index.vue")
      }
    ]
  },
  {
    path: PathEnum.STRATEGY,
    component: Layout,
    meta: { icon: 'setting', title: '升级策略' },
    children: [
      {
        path: PathEnum.STRATEGY,
        component: () => import("@/views/strategy/index.vue")
      }
    ]
  },
  {
    path: PathEnum.INIT_ADMIN,
    meta: { hidden: true },
    component: () => import('@/views/init/index.vue')
  },
  {
    path: "*",
    meta: { hidden: true },
    redirect: PathEnum.HOME
  }
];

const router = new VueRouter({
  // mode: "history",
  // base: process.env.BASE_URL,
  routes: Routes
});

export default router;

export const columns: any[] = [
  {
    title: "时间",
    dataIndex: "createTime",
    sorter: true,
    width: '170px',
    scopedSlots: { customRender: "createTime" }
  },
  {
    title: "系统类型",
    dataIndex: "osType",
    width: '100px',
    scopedSlots: { customRender: "osType" }
  },
  {
    title: "驱动类型",
    dataIndex: "driverType",
    width: '120px',
    scopedSlots: { customRender: "driverType" }
  },
  {
    title: "系统位数",
    dataIndex: "cpuBits",
    width: '100px',
    scopedSlots: { customRender: "cpuBits" }
  },
  {
    title: "驱动名称",
    dataIndex: "fileName",
    scopedSlots: { customRender: "fileName" },
    width: '200px'
  },
  {
    title: "版本信息",
    dataIndex: "version",
    scopedSlots: { customRender: "version" },
    width: '200px'
  },
  {
    title: "变更履历",
    dataIndex: "description",
    width: "30%",
    scopedSlots: { customRender: "description" }
  },
  {
    title: "操作",
    align: "center",
    width: "200px",
    scopedSlots: { customRender: "handler" }
  }
];

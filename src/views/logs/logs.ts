export const columns: any[] = [
  {
    title: "时间",
    dataIndex: "createTime",
    sorter: true,
    width: '170px',
    scopedSlots: { customRender: "createTime" }
  },
  {
    title: "用户名",
    dataIndex: "userName"
  },
  {
    title: "主机名",
    dataIndex: "hostName"
  },
  {
    title: "系统类型",
    dataIndex: "osType",
    scopedSlots: { customRender: "osType" }
  },
  {
    title: "系统版本",
    dataIndex: "osVer"
  },
  {
    title: "驱动类型",
    dataIndex: "driverType",
    scopedSlots: { customRender: "driverType" }
  },
  {
    title: "系统位数",
    dataIndex: "cpuBits",
    scopedSlots: { customRender: "cpuBits" }
  },
  {
    title: "升级前版本",
    dataIndex: "oldVer"
  },
  {
    title: "升级后版本",
    dataIndex: "newVer"
  },
  {
    title: "结果",
    dataIndex: "result",
    scopedSlots: { customRender: "result" }
  },
  {
    title: "操作",
    align: "center",
    width: "150px",
    scopedSlots: { customRender: "handler" }
  }
];

import { InstallMethod } from "@/utils/VEnum";
/* 策略 */
export interface IStrategy {
  _id: string /* 主键 */;
  method: InstallMethod /* 安装方式 */;
  timeInterval: number /* 间隔时间 */;
}

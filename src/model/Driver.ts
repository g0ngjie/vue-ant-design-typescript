import { OsType, DriverType, CpuBits } from "@/utils/VEnum";
export interface IDriver {
  _id: string /* 主键 */;
  createTime: string /* 驱动上传时间 */;
  osType: OsType /* 系统类型 */;
  driverType: DriverType /* 驱动类型 */;
  cpuBits: CpuBits /* 系统位数 */;
  fileName?: string /* 驱动名称 */;
  fileSize: number /* 驱动大小 */;
  version: string /* 版本类型 */;
  description?: string /* 变更履历 */;
  isNewest: boolean /* 是否是最新版本 */;
  httpPath: string /* 驱动静态资源的映射地址 */;
  md5?: string /* md5文件加密標示 */;
  filePath: string /* 文件上传路径 */;
}

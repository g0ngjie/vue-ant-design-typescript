export interface IRespones {
  errNo: number /* 服务端返回错误码 */;
  msg: string | null /* 错误信息 */;
  item?: any /* <T> */;
}

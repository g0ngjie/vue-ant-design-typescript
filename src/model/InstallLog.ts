import { DriverType, OsType, CpuBits, UpgradeResult } from "@/utils/VEnum";

export interface IDriver {
  _id: string;
  createTime: string /* 驱动安装时间 */;
  userName: string /* pc端用户名 */;
  hostName?: string /* pc端主机名 */;
  domainName?: string /* pc端域名 */;
  version?: string /* 升级包的版本号 */;
  driverType: DriverType /* 驱动类型 */;
  osType: OsType /* 系统类型 */;
  cpuBits: CpuBits /* 位数 */;
  osVer?: string /* 当前操作系统版本 */;
  oldVer: string /* 升级前的版本 */;
  newVer: string /* 升级后的版本 */;
  result: UpgradeResult /* 安装结果 */;
  httpPath: string /* 失败文件存储地址(静态资源) */;
}

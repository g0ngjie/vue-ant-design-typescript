import request from "@/common/request";
import { PREFIX } from "./cfg";
import { AxiosPromise } from 'axios';
import { IRespones } from '@/model/Response';

/**
 * 设置驱动的相关信息
 * @param data
 */
export function drive_upload(data: any): AxiosPromise<IRespones> {
  return request({
    url: `${PREFIX}/driver/upload`,
    method: "post",
    data
  });
}

/**
 * 编辑驱动内容
 * @param id
 * @param data
 */
export function drive_edit(id: string, data: any) {
  return request({
    url: `${PREFIX}/driver/upload/${id}`,
    method: "put",
    data
  });
}

/**
 * 删除驱动
 * @param id
 */
export function drive_delete(id: string) {
  return request({
    url: `${PREFIX}/driver/upload/${id}`,
    method: "delete"
  });
}

/**
 * 获取驱动列表
 * @param data
 */
export function drive_pages(data: any): AxiosPromise<IRespones> {
  return request({
    url: `${PREFIX}/driver/drivers`,
    method: "post",
    data
  });
}

/**
 * 计算驱动列表个数
 * @param data
 */
export function drive_count(data: any): AxiosPromise<IRespones> {
  return request({
    url: `${PREFIX}/driver/drivers/count`,
    method: "post",
    data
  });
}

/**
 * 校验文件上传是否完成
 * @param data
 */
export function drive_finish(data: any): AxiosPromise<IRespones> {
  return request({
    url: `${PREFIX}/driver/finish`,
    method: "post",
    data
  });
}

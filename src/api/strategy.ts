import request from "@/common/request";
import { PREFIX } from "./cfg";
import { AxiosPromise } from 'axios';
import { IRespones } from '@/model/Response';

/**
 * 获取升级策略
 */
export function strategy(): AxiosPromise<IRespones> {
  return request({
    url: `${PREFIX}/strategy`,
    method: "get"
  });
}

/**
 * 设置升级策略
 * @param data
 */
export function strategy_set(data: any): AxiosPromise<IRespones> {
  return request({
    url: `${PREFIX}/strategy/set`,
    method: "post",
    data
  });
}

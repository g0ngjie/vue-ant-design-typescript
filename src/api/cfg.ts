/**
 * 1、前后不分离，build后打包到server里使用
 * 2、nginx需要改为常量 例： /api
 */
export const PREFIX: string =
  process.env.NODE_ENV === "development" ? "/api" : "";

import request from "@/common/request";
import { PREFIX } from "./cfg";
import { AxiosPromise } from 'axios';
import { IRespones } from '@/model/Response';

/**
 * 登录
 * @param data
 */
export function user_login(data: any): AxiosPromise<IRespones> {
  return request({
    url: `${PREFIX}/user/login`,
    method: "post",
    data
  });
}

/**
 * 修改密码
 * @param data
 */
export function user_passwd(data: any): AxiosPromise<IRespones> {
  return request({
    url: `${PREFIX}/user/passwd`,
    method: "put",
    data
  });
}

/**
 * 添加管理员
 * @param data
 */
export function user_admin(data: any): AxiosPromise<IRespones> {
  return request({
    url: `${PREFIX}/user/admin`,
    method: "post",
    data
  });
}

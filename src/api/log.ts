import request from "@/common/request";
import { PREFIX } from "./cfg";
import { AxiosPromise } from 'axios';
import { IRespones } from '@/model/Response';

/**
 * 获取安装列表
 * @param {*} data
 */
export function log_pages(data: any): AxiosPromise<IRespones> {
  return request({
    url: `${PREFIX}/driver/installlogs`,
    method: "post",
    data
  });
}

/**
 * 获取安装列表个数
 * @param data
 */
export function log_count(data: any): AxiosPromise<IRespones> {
  return request({
    url: `${PREFIX}/driver/installlogs/count`,
    method: "post",
    data
  });
}

/**
 *获取安装版本
 * @export
 */
export function log_installversions(): AxiosPromise<IRespones> {
  return request({
    url: `${PREFIX}/driver/installversions`,
    method: "post"
  });
}

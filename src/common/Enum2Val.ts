import {
  OsType,
  DriverType,
  CpuBits,
  UpgradeResult,
  SortBy,
  InstallMethod
} from "@/utils/VEnum";

/* 驱动类型 枚举 对照 */
export const DriverType2Val = {
  [DriverType.DIRECT]: "直接打印驱动",
  [DriverType.ROAMING]: "云打印驱动"
};

/* 操作系统类型 枚举 对照 */
export const OsType2Val = {
  [OsType.WIN]: "Windows",
  [OsType.MAC]: "MacOS"
};

/* 系统位数 */
export const CpuBits2Val: any = {
  [CpuBits.WIN32]: "32位",
  [CpuBits.WIN64]: "64位",
  [CpuBits.GENERAL]: "通用"
};

/* 升级结果 */
export const Result2Val: any = {
  [UpgradeResult.ERROR]: "失败",
  [UpgradeResult.SUCCESS]: "成功"
};

/* 排序 */
export const SortBy2Val: any = {
  ascend: SortBy.ASC,
  descend: SortBy.DESC
};

/* 驱动升级方式 */
export const Method2Val: any = {
  [InstallMethod.M0]: "提示用户升级，确定后下载升级包",
  [InstallMethod.M1]: "自动下载升级包，再提示用户升级",
  [InstallMethod.M2]: "不提示用户，强制启动升级"
};

/**
 * 校验 init 登录账户
 * max 30
 * min 5
 * @param rule
 * @param value
 * @param callback
 * @description 账号组成规则：“a-z, A-Z,0-9,-,_,.，@”区分大小写，首字母只能是英文字母。用户名必须5-30位。
 */
export const validAccount = (rule: any, value: string, callback: any) => {
  if (!value) return callback();
  const re: any = /^[a-zA-Z]([a-zA-z0-9]|[.@_-])*$/;
  const max: number = 31;
  const min: number = 4;
  if (re.test(value)) {
    if (value.length < max && value.length > min) {
      return callback();
    }
  }
  return callback("该输入项不合法");
};

/**
 * 校验密码输入规则
 * max 20
 * min 6
 * @param rule
 * @param value
 * @param callback
 * @description 密码组成规则：“a-z,A-Z,0-9，-,_,@,.”，区分大小写，密码必须6-20位。
 */
export const validPasswd = (rule: any, value: string, callback: any) => {
  if (!value) return callback();
  const max: number = 21;
  const min: number = 5;
  const re = /^[a-zA-Z0-9.@_-]*$/;
  if (re.test(value)) {
    if (value.length < max && value.length > min) {
      return callback();
    }
  }
  return callback("该输入项不合法");
};

/**
 * 校验版本信息输入规则
 * @param rule
 * @param value
 * @param callback
 * @description 版本信息组成规则：“0-9，.”，区分大小写。
 */
export const validVersion = (rule: any, value: string, callback: any) => {
  if (!value) return callback();
  const re = /^(\d{1,2})(\.)(\d{1,2})(\.)(\d{1,2})(\.)(\d{1,2})$/;
  if (re.test(value)) {
    return callback();
  }
  return callback("只能为“0-9，.”，且格式为：xx.xx.xx.xx");
};

import axios from "axios";
import Ant from "ant-design-vue";
const { message } = Ant;
import Error from "@/utils/Error";
import { LocalStorage as LocalEnum } from "@/utils/VEnum";

const service = axios.create({
  timeout: 5000
});

// request interceptor
service.interceptors.request.use(
  config => {
    // Do something before request is sent
    const token = localStorage.getItem(LocalEnum.TOKEN.toString()) || "";
    if (token) {
      config.headers["token"] = token;
    }
    return config;
  },
  error => {
    Promise.reject(error);
  }
);

// response interceptor
service.interceptors.response.use(
  response => response,
  error => {
    const { status } = error.response;
    const { errNo, msg } = error.response.data;
    const err: any = Error;
    message.error(err[errNo] || "系统异常");
    //debug
    // console.log(">> errNo", errNo, ",msg", msg, "<<");
    return Promise.reject(error.response.data);
  }
);

export default service;

import {
  DriverType,
  OsType,
  UpgradeResult,
  CpuBits,
  InstallMethod
} from "@/utils/VEnum";

const { DIRECT, ROAMING } = DriverType;
const { WIN, MAC } = OsType;
const { SUCCESS, ERROR } = UpgradeResult;
const { WIN32, WIN64, GENERAL } = CpuBits;
const { M0, M1, M2 } = InstallMethod;

export const selectColumns = {
  driverType: [
    {
      label: "直接打印驱动",
      value: DIRECT
    },
    {
      label: "云打印驱动",
      value: ROAMING
    }
  ],
  osType: [
    {
      label: "Windows",
      value: WIN
    },
    {
      label: "MacOS",
      value: MAC
    }
  ],
  upgrade: [
    {
      label: "成功",
      value: SUCCESS
    },
    {
      label: "失败",
      value: ERROR
    }
  ],
  cpuBits: [
    {
      label: "通用",
      value: GENERAL
    },
    {
      label: "win32",
      value: WIN32
    },
    {
      label: "win64",
      value: WIN64
    }
  ],
  method: [
    //驱动升级方式
    {
      label: "提示用户升级，确定后下载升级包",
      value: M0
    },
    {
      label: "自动下载升级包，再提示用户升级",
      value: M1
    },
    {
      label: "不提示用户，强制启动升级",
      value: M2
    }
  ]
};

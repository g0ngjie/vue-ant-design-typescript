import { VuexModule, Module, Mutation, Action } from "vuex-module-decorators";

@Module({ name: "App" })
export default class App extends VuexModule {}

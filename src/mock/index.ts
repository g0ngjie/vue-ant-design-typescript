
import Mock from 'mockjs'
import * as LogApi from './logs'

Mock.mock(/\/api\/log\/page/, 'post', LogApi.pages)

export default Mock

import Mock from "mockjs";
const Random = Mock.Random; // Mock.Random 是一个工具类，用于生成各种随机数据
/**
 * 驱动类型
 */
enum DriverTypeEnum {
  Direct, //直打
  Roaming, //漫游
  Other //其他
}

/**
 * 操作系统位数
 */
enum CpuBits {
  BigBit, //64位
  SmallBit //32位
}

/**
 * 操作系统类型
 */
enum OsType {
  Window,
  MacOS,
  Andriod
}

/**
 * @module LogModule
 */
interface LogModule {
  _id: string;
  description: string;
  fileName: string;
  version: string;
  driverType: DriverTypeEnum;
  cpuBits: CpuBits;
  osType: OsType;
  createTime: string;
}

async function findOneLog(): Promise<LogModule> {
  const log: LogModule = Mock.mock({
    _id: "@guid",
    description: "@url(http)",
    fileName: "@cname",
    version: Random.sentence(3, 5),
    driverType: DriverTypeEnum.Direct,
    cpuBits: CpuBits.BigBit,
    osType: OsType.MacOS,
    createTime: Random.date("yyyy-MM-dd")
  });

  return log;
}

/**
 * log 分页
 * @returns 50limit
 */
export const pages = async () => {
  const limit: number = 30;
  const logs: LogModule[] = [];
  const count: number = limit;
  for (let i = 0; i < limit; i++) {
    const log: LogModule = await findOneLog();
    logs.push(log);
  }
  return { errNo: 0, item: logs, count };
};

import { IntegerRandom } from "@/utils";
import Mock from "mockjs";
const Random = Mock.Random; // Mock.Random 是一个工具类，用于生成各种随机数据
/**
 * 驱动类型
 */
enum DriverTypeEnum {
  Direct, //直打
  Roaming, //漫游
  Other //其他
}

/**
 * 操作系统位数
 */
enum CpuBits {
  BigBit, //64位
  SmallBit //32位
}

/**
 * 操作系统类型
 */
enum OsType {
  Window,
  MacOS,
  Andriod
}

/**
 * @module LogModule
 */
interface LogModule {
  hostName: string; //PC端主机名
  userName: string; //PC端用户名
  version: string; //升级包的版本号
  driverType: DriverTypeEnum;
  cpuBits: CpuBits;
  osType: OsType;
  createTime: string;
}

async function findOneLog(): Promise<LogModule> {
  const log: LogModule = Mock.mock({
    hostName: "@url(http)",
    userName: "@cname",
    version: Random.sentence(3, 5),
    driverType: DriverTypeEnum.Direct,
    cpuBits: CpuBits.BigBit,
    osType: OsType.MacOS,
    createTime: Random.date("yyyy-MM-dd")
  });

  return log;
}

/**
 * log 分页
 */
export const pages = async () => {
  let limit: number = 20;
  const logs: LogModule[] = [];
  let count: number = 0;
  for (let i = 0; i < limit; i++) {
    const log: LogModule = await findOneLog();
    logs.push(log);
    count = i;
    limit--;
  }
  return { errNo: 0, item: logs, count };
};

module.exports = {
  /**
   * 默认情况下，ESLint 会在所有父级目录里寻找配置文件，一直到根目录。如果你想要你所有项目都遵循一个特定的约定时，这将会很有用，但有时候会导致意想不到的结果。
   * 为了将 ESLint 限制到一个特定的项目，在你项目根目录下的 package.json 文件或者 .eslintrc.* 文件里的 eslintConfig 字段下设置 "root": true。
   * ESLint 一旦发现配置文件中有 "root": true，它就会停止在父级目录中寻找。
   */
  root: true,
  /**
   * 指定你想启用的环境
   */
  env: {
    //要额外支持新的 ES6 全局变量
    es6: true,
    node: true
  },
  /**
   * 开启推荐配置信息
   */
  extends: ["plugin:vue/essential", "eslint:recommended", "@vue/typescript"],
  /**
   * 启用的规则及各自的错误级别
   */
  rules: {
    "no-console": process.env.NODE_ENV === "production" ? 2 : 0, // 禁止用console
    "no-debugger": process.env.NODE_ENV === "production" ? 2 : 0,
    "space-before-function-paren": [
      //function关键字与开始参数之间允许有空格
      "error",
      {
        anonymous: "always", //是用于匿名函数表达式 （例如function () {}）。

        named: "never", //是用于命名函数表达式 （例如function foo () {}）。

        asyncArrow: "always" //是用于异步箭头函数表达式（例如async () => {}）。
      }
    ],
    camelcase: [
      //强制驼峰法命名
      0,
      {
        properties: "always"
      }
    ],
    "comma-dangle": [2, "never"], //（对象最后一项省略逗号）
    "comma-spacing": [
      // 控制逗号前后的空格
      2,
      {
        before: false,
        after: true
      }
    ],
    "comma-style": [2, "last"], // 控制逗号在行尾出现还是在行首出现
    "constructor-super": 2, // 强制在子类构造函数中用super()调用父类构造函数，TypeScrip的编译器也会提示
    "key-spacing": [
      //对象字面量中冒号的前后空格
      2,
      {
        beforeColon: false,
        afterColon: true
      }
    ],
    "keyword-spacing": [
      //if (condition) { ... }   // ✓ ok
      2,
      {
        before: true,
        after: true
      }
    ],
    eqeqeq: 0, // 使用 === 替代 ==
    "handle-callback-err": 0, //nodejs 处理错误
    "new-parens": 2, //new时必须加小括号
    "no-const-assign": 2, //禁止修改const声明的变量
    "no-dupe-args": 2, //函数参数不能重复
    "no-dupe-class-members": 2, //不允许类中出现重复的声明
    "no-dupe-keys": 2, //在创建对象字面量时不允许键重复 {a:1,a:1}
    "no-duplicate-case": 2, //switch中的case标签不能重复
    "no-extra-bind": 2, //禁止不必要的函数绑定
    "no-extra-boolean-cast": 2, //禁止不必要的bool转换
    "no-extra-parens": [2, "functions"], //禁止非必要的括号
    "no-fallthrough": 2, //禁止switch穿透
    "no-floating-decimal": 2, //禁止省略浮点数中的0 .5 3.
    "no-func-assign": 2, //禁止重复的函数声明
    "no-implied-eval": 2, //禁止使用隐式eval
    "no-inner-declarations": [2, "functions"], //禁止在块语句中使用声明（变量或函数）
    "no-invalid-regexp": 2, //禁止无效的正则表达式
    "no-irregular-whitespace": 2, //不能有不规则的空格
    "no-iterator": 2, //禁止使用__iterator__ 属性
    "no-label-var": 2, //label名不能与var声明的变量名相同
    "no-labels": [
      2,
      {
        //禁止标签声明
        allowLoop: false,
        allowSwitch: false
      }
    ],
    "no-lone-blocks": 2, //禁止不必要的嵌套块
    "no-multi-spaces": 2, //不能用多余的空格
    "no-multiple-empty-lines": [
      2,
      {
        max: 1
      }
    ], //空行最多不能超过1行
    "no-obj-calls": 0, //不能调用内置的全局对象，比如Math() JSON()
    "no-redeclare": 2, // 在同一个作用域中禁止多次重复定义
    "no-spaced-func": 2, //函数调用时 函数名与()之间不能有空格
    "no-sparse-arrays": 2, //禁止稀疏数组， [1,,2]
    "no-this-before-super": 2, //在调用super()之前不能使用this或super
    "no-throw-literal": 2, //禁止抛出字面量错误 throw "error";
    "no-trailing-spaces": 2, //一行结束后面不要有空格
    "no-undef": 2, //不能有未定义的变量
    "no-unexpected-multiline": 0, //避免多行表达式
    "no-unmodified-loop-condition": 0, //检查引用是否在循环中被修改
    "no-unneeded-ternary": 0, //禁止不必要的嵌套 var isYes = answer === 1 ? true : false;
    "no-unreachable": 2, //不能有无法执行的代码
    "no-unsafe-finally": 0,
    "no-useless-call": 2, //禁止不必要的call和apply
    "no-useless-computed-key": 0, //没有必要使用带文字的计算属性
    "no-useless-constructor": 2, //可以在不改变类的工作方式的情况下安全地移除的类构造函数
    "no-useless-escape": 0,
    "no-whitespace-before-property": 0,
    "no-with": 2, //禁用with
    "one-var": 0, //连续声明
    "operator-linebreak": [
      2,
      "after",
      {
        overrides: {
          "?": "before",
          ":": "before"
        }
      }
    ], //换行时运算符在行尾还是行首
    "padded-blocks": 0, //块语句内行首行尾是否要空行
    quotes: 0, //[1, 'double'], //引号类型 `` "" ''
    semi: [0, "never"], // 禁止用分号
    "semi-spacing": [
      2,
      {
        before: false,
        after: true
      }
    ], //分号前后空格
    "space-in-parens": [2, "never"], //小括号里面要不要有空格
    "space-infix-ops": 2, //中缀操作符周围要不要有空格
    "space-unary-ops": [
      2,
      {
        words: true,
        nonwords: false
      }
    ], //一元运算符的前/后要不要加空格
    "spaced-comment": 0, //注释风格不要有空格什么的
    "use-isnan": 2, //禁止比较时使用NaN，只能用isNaN()
    "valid-typeof": 2, //必须使用合法的typeof的值
    "wrap-iife": [2, "any"], //立即执行函数表达式的小括号风格
    "prefer-const": 2, //首选const
    "object-curly-spacing": [
      2,
      "always",
      {
        objectsInObjects: false
      }
    ], //大括号内是否允许不必要的空格
    "array-bracket-spacing": [2, "never"], //是否允许非空数组里面有多余的空格
  },
  /**
   * 设置解析器选项（必须设置这个属性）
   */
  parserOptions: {
    parser: "@typescript-eslint/parser",
    //启用 ES9 额外语法支持
    ecmaVersion: 9
  },
  overrides: [
    {
      files: ["**/__tests__/*.{j,t}s?(x)"],
      env: {
        mocha: true
      }
    }
  ]
};
